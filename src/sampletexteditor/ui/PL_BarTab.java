/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sampletexteditor.ui;

import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.FontUIResource;

/**
 *
 * @author pedroluis
 */
public class PL_BarTab extends JPanel {
    
    private JLabel jlIcono;
    private JLabel jlTitulo;
    private JButton jbCerrar;
    private FlowLayout fLayout;
    private PL_PanelTabs parent;
    
    public PL_BarTab(ImageIcon icono, String titulo, PL_PanelTabs parent,
            int position) {
        fLayout = new FlowLayout();
        fLayout.setVgap(2);
        setLayout(fLayout);
        jlIcono = new JLabel(icono, JLabel.CENTER);
        jlTitulo = new JLabel(titulo);
        setFont(new FontUIResource("Dialog", Font.BOLD, 10));
        jlTitulo.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jbCerrar = new PL_ButtonClose(parent, position);
        
        add(jlIcono); add(jlTitulo); add(jbCerrar);
        
        setOpaque(false);
    }
    
    public void setTabBarTitulo(String titulo) {
        jlTitulo.setText(titulo);
    }

    public String getTabBarTitulo() {
        return jlTitulo.getText();
    }
    
}

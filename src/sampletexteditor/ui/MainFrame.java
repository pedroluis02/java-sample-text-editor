/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sampletexteditor.ui;

/**
 *@author Pedro Luis
**/

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import sampletexteditor.utils.PL_Resources;

public class MainFrame extends JFrame {
    
    private PL_PanelTabs p = new PL_PanelTabs();
 
    public MainFrame(String title) {
    	super(title);
    	this.setSize(this.getScreen().width/2 + 200, 
                this.getScreen().height/2 + 200);
    	this.setLocation(this.getScreen().width/4 - 100, 
                this.getScreen().height/4 - 100);
    	this.setJMenuBar(p.barM_PL());
    	this.add(p.barH_PL(), BorderLayout.NORTH);
    	this.add(p, BorderLayout.CENTER);
        this.add(p.barE_PL(), BorderLayout.SOUTH);
        this.add(new JPanel(), BorderLayout.WEST);
        this.add(new JPanel(), BorderLayout.EAST);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                          PL_Resources.getIconResource("image.jpg")));
    	this.setVisible(true);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
               boolean s = p.notFileSaved();
	       if(s) {
                    System.exit(EXIT_ON_CLOSE);
                }
	    }
        });
    } 

    private Dimension getScreen() {
    	return Toolkit.getDefaultToolkit().getScreenSize();
    }
    
}

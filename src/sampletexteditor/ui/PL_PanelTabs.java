/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sampletexteditor.ui;

/**
 *
 * @author Pedro Luis
 */

import sampletexteditor.filters.PL_TextFileFilter;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.BorderUIResource;
import sampletexteditor.utils.PL_Aditional;
import sampletexteditor.utils.PL_Constants;
import sampletexteditor.utils.PL_Resources;

public class PL_PanelTabs extends JTabbedPane 
    implements ActionListener, ChangeListener {
    private ArrayList <PL_AreaText> tabsAreaText;
    private ArrayList <String> directories;

    private JFileChooser fileDialog;

    private Integer tabCreated;

    private JPopupMenu menuContex;
    private JMenuItem fileContex[]; 

    private JMenuBar barM;
    private JMenu menu[];
    private JMenuItem file[];
    private JMenuItem edit[];
    private JCheckBoxMenuItem view[];

    private JToolBar barH;
    private JToggleButton []button;
    
    private JPanel barE;
    private JSlider zoom;
    private JTextField dir;

    private ImageIcon iconPL;
    private int idIncrement;

    public PL_PanelTabs() {
    	this.initComponents();
    }

    private void initComponents() {
    	tabsAreaText = new ArrayList <PL_AreaText> ();
    	directories = new ArrayList <String> ();
    	fileDialog = new JFileChooser();fileDialog.setMultiSelectionEnabled(false);
        FileFilter defaultFF = fileDialog.getFileFilter();
        fileDialog.removeChoosableFileFilter(fileDialog.getFileFilter());
        fileDialog.addChoosableFileFilter(new PL_TextFileFilter());
        fileDialog.addChoosableFileFilter(defaultFF);
        //files.setFileFilter(new PL_TextFileFilter());
        iconPL = new ImageIcon(PL_Resources.getIconResource("pl.gif"));
    	tabCreated = 0;
        idIncrement = 0;
    	this.initMenuBar();
    	this.initToolBar();
        this.initStatusBar();
    	this.initMenuContex();
    	this.setComponentPopupMenu(this.getMenuContext());
        this.addChangeListener(this);
        
        this.newTabAT();
    }
    
    /*******Init Bars*********/
    
    private void initMenuBar() {//init Menu Bar
    	barM = new JMenuBar();

    	menu = new JMenu[4];

    	menu[0] = new JMenu("File");
    	menu[0].setMnemonic('F');

    	file = new JMenuItem[6];

    	file[0] = new JMenuItem("New");file[0].setActionCommand("new");
    	file[0].setIcon(new ImageIcon(PL_Resources.getIconResource("New16.gif")));
    	file[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
    	file[0].addActionListener(this);

    	file[1] = new JMenuItem("Open");file[1].setActionCommand("open");
    	file[1].setIcon(new ImageIcon(PL_Resources.getIconResource("Open16.gif")));
    	file[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                InputEvent.CTRL_MASK));
    	file[1].addActionListener(this);

    	file[2] = new JMenuItem("Save");file[2].setActionCommand("save");
        file[2].setIcon(new ImageIcon(PL_Resources.getIconResource("Save16.gif")));
    	file[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, 
                InputEvent.CTRL_MASK));
    	file[2].addActionListener(this);

    	file[3] = new JMenuItem("Save as");file[3].setActionCommand("save as");
        file[3].setIcon(new ImageIcon(PL_Resources.getIconResource("Save-as16.gif")));
    	file[3].addActionListener(this);

    	file[4] = new JMenuItem("Close");file[4].setActionCommand("close");
        file[4].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, 
                InputEvent.CTRL_MASK));
    	file[4].setIcon(new ImageIcon(PL_Resources.getIconResource("Delete16.gif")));
    	file[4].addActionListener(this);

    	file[5] = new JMenuItem("Exit");file[5].setActionCommand("exit");
    	file[5].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, 
                InputEvent.CTRL_MASK));
        file[5].setIcon(
                new ImageIcon(PL_Resources.getIconResource("Exit16.gif")));
    	file[5].addActionListener(this);

    	menu[1] = new JMenu("Edit");menu[1].setMnemonic('E');

    	edit = new JMenuItem[4];

    	edit[0] = new JMenuItem("Copy");edit[0].setActionCommand("copy");
        edit[0].setIcon(new ImageIcon(PL_Resources.getIconResource("Copy16.gif")));
    	edit[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
    	edit[0].addActionListener(this);

    	edit[1] = new JMenuItem("Cut");edit[1].setActionCommand("cut");
        edit[1].setIcon(new ImageIcon(PL_Resources.getIconResource("Cut16.gif")));
    	edit[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
    	edit[1].addActionListener(this);

    	edit[2] = new JMenuItem("Paste");edit[2].setActionCommand("paste");
        edit[2].setIcon(new ImageIcon(PL_Resources.getIconResource("Paste16.gif")));
    	edit[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
    	edit[2].addActionListener(this);

        edit[3] = new JMenuItem("Select All");edit[2].setActionCommand("all");
    	edit[3].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
    	edit[3].addActionListener(this);

        menu[2] = new JMenu("View");menu[2].setMnemonic('V');

        view = new JCheckBoxMenuItem [3];

        view[0] = new JCheckBoxMenuItem ("Menu Bar"); view[0].setActionCommand("menu");
        view[0].setSelected(true);view[0].addActionListener(this);
        
        view[1] = new JCheckBoxMenuItem ("Tool Bar");view[1].setActionCommand("tool");
        view[1].setSelected(true);view[1].addActionListener(this);

        view[2] = new JCheckBoxMenuItem ("Status Bar");view[2].setActionCommand("status");
        view[2].setSelected(true);view[2].addActionListener(this);

        menu[3] = new JMenu("Help");menu[3].setMnemonic('H');
    }

    private void initToolBar() {//Init ToolBar
    	barH = new JToolBar(JToolBar.HORIZONTAL);
    	barH.setFloatable(false);

    	button = new JToggleButton[5];

    	button[0] = new JToggleButton(new ImageIcon(PL_Resources.getIconResource("New24.gif")));
    	button[0].setToolTipText("New");button[0].addActionListener(this);
        button[0].setActionCommand("new");

    	button[1] = new JToggleButton(new ImageIcon(PL_Resources.getIconResource("Open24.gif")));
        button[1].setToolTipText("Open");button[1].setActionCommand("open");
    	button[1].addActionListener(this);

    	button[2] = new JToggleButton(new ImageIcon(PL_Resources.getIconResource("Save24.gif")));
    	button[2].setToolTipText("Save");button[2].setActionCommand("save");
        button[2].addActionListener(this);

    	button[3] = new JToggleButton(new ImageIcon(PL_Resources.getIconResource("Save-as24.gif")));
        button[3].setToolTipText("Save as");button[3].setActionCommand("save as");
        button[3].addActionListener(this);

    	button[4] = new JToggleButton(new ImageIcon(PL_Resources.getIconResource("Delete24.gif")));
        button[4].setToolTipText("Close");button[4].setActionCommand("close");
    	button[4].addActionListener(this); 
    }

    private void initStatusBar() {// Init StatusBar
        barE = new JPanel(new FlowLayout(FlowLayout.CENTER));
        barE.setBorder(BorderUIResource.getRaisedBevelBorderUIResource());
        
        dir = new JTextField(); dir.setEditable(false);
        
        zoom = new JSlider(JSlider.HORIZONTAL, 15, 200, 15);
        zoom.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(getSelectedIndex()>=0) {
                    tabsAreaText.get(getSelectedIndex()).setLengthFont(zoom.getValue());
                }
            }
        });
    }

    private void initMenuContex() {//Init menuContext
    	fileContex = new JMenuItem[3];

    	menuContex = new JPopupMenu();

    	fileContex[0] = new JMenuItem("Save");
        fileContex[0].setActionCommand("save");
        fileContex[0].setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
    	fileContex[0].setIcon(
                new ImageIcon(PL_Resources.getIconResource("Save16.gif")));
        fileContex[0].addActionListener(this);

    	fileContex[1] = new JMenuItem("Save as");
        fileContex[1].setActionCommand("save as");
        fileContex[1].setIcon(
                new ImageIcon(PL_Resources.getIconResource("Save-as16.gif")));
        fileContex[1].addActionListener(this);

    	fileContex[2] = new JMenuItem("Close");
        fileContex[2].setActionCommand("close");
        fileContex[2].setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
    	fileContex[2].setIcon(
                new ImageIcon(PL_Resources.getIconResource("Delete16.gif")));
        fileContex[2].addActionListener(this);
    }

    public JMenuBar barM_PL() {//Add menus to menuBar
    	menu[0].add(file[0]);menu[0].add(file[1]);menu[0].add(new JSeparator());
    	menu[0].add(file[2]);menu[0].add(file[3]);menu[0].add(new JSeparator());
    	menu[0].add(file[4]);menu[0].add(new JSeparator());menu[0].add(file[5]);

    	menu[1].add(edit[0]);menu[1].add(edit[1]);menu[1].add(edit[2]);
        menu[1].add(new JSeparator());menu[1].add(edit[3]);

        //menu[2].add(view[0]);
        menu[2].add(view[1]);menu[2].add(view[2]);

    	barM.add(menu[0]);barM.add(menu[1]);
        barM.add(menu[2]);barM.add(menu[3]);
        
    	return barM;
    }

    public JToolBar barH_PL() {// Add Buttons to ToolBar
    	barH.add(button[0]);barH.addSeparator();
        barH.add(button[1]);barH.addSeparator();
        barH.add(button[2]);barH.addSeparator();
    	barH.add(button[3]);barH.addSeparator();
        barH.add(button[4]);
    	return barH; 
    }

    public JPanel barE_PL() {//Add Components to StatusBa
        barE.add(new JLabel("Dir:"));barE.add(dir);
        barE.add(new JLabel("15"));barE.add(zoom);
        barE.add(new JLabel("100%"));
        return barE;
    }
    
    private JPopupMenu getMenuContext() {//Add menus to menuContext
    	menuContex.add(fileContex[0]);menuContex.add(fileContex[1]);
    	menuContex.add(fileContex[2]);
    	return menuContex;
    }
    /****Operations with Tabs*/
    private void newTabTA(String dir, String subtitle) {//Creates new Tabs with name and a directory
    	tabsAreaText.add(new PL_AreaText(this, idIncrement)); 
        directories.add(dir);
        this.dir.setText(dir);
        tabsAreaText.get(tabsAreaText.size()-1).setState(true);
    	this.addTab(subtitle, iconPL,
                new JScrollPane(tabsAreaText.get(tabsAreaText.size()-1)));
        PL_BarTab bt = new PL_BarTab(iconPL, subtitle, this, idIncrement);
        
        setTabComponentAt(tabsAreaText.size()-1, bt);
        idIncrement++;
    }

    private void newTabAT() {//Creates new default tab.
    	tabsAreaText.add(new PL_AreaText(this, idIncrement)); directories.add("No saved");
        String title = "default" + (tabCreated++) + "." + PL_Constants.EXT_FILES + "*";
    	this.addTab(title, iconPL,
                new JScrollPane(tabsAreaText.get(tabsAreaText.size()-1)));
        PL_BarTab bt = new PL_BarTab(iconPL, title, this, idIncrement);
        
        setTabComponentAt(tabsAreaText.size()-1, bt);
        idIncrement++;
    }

    public void removeTabAT(int pos) { //Remove a tabs and directory 
    	this.removeTabAt(pos);
    	this.tabsAreaText.remove(pos);
        this.directories.remove(pos);
    }
    
    private void newSave(File fo, int select) {//Save content to the tab in a file.
        try {
            BufferedWriter bw = 
                    new BufferedWriter(new FileWriter(fo.getAbsolutePath()));
            String text = tabsAreaText.get(select).getText();
            directories.add(select, fo.getAbsolutePath());
            tabsAreaText.get(select).setState(true);
            PL_BarTab tb = (PL_BarTab)getTabComponentAt(select);
            tb.setTabBarTitulo(fo.getName());
            dir.setText(fo.getAbsolutePath()); 
           file[2].setEnabled(false); button[2].setEnabled(false);
            bw.write(text);
            bw.close();
        }
        catch(IOException ex) {
            JOptionPane.showMessageDialog(this, 
                            "Save file", "Error in save file", 0);
        }
    }
    private void saveTabV(int select) {//Save the chages made to the file
        try{
            String dirt = directories.get(select);
            BufferedWriter bw = new BufferedWriter(new FileWriter(dirt));
            String text = tabsAreaText.get(select).getText();
            tabsAreaText.get(select).setState(true);
            file[2].setEnabled(false); button[2].setEnabled(false);
            bw.write(text);
            bw.close();
        }
        catch(IOException ex){
            JOptionPane.showMessageDialog(this, 
                            "Save file", "Error in save data", 0);
        }
    }
    public int closeTabV(int select) {
        PL_BarTab tb = (PL_BarTab)getTabComponentAt(select);
        String title = tb.getTabBarTitulo();
        if((title.charAt(title.length()-1) == '*' && 
           tabsAreaText.get(select).getText().length() == 0) ||
                (title.charAt(title.length()-1) != '*')) {
           this.removeTabAT(select);
           if(this.getTabCount() == 0) {
                 System.exit(0);
           }
           return -1;
        }

        int conf = JOptionPane.showConfirmDialog(this,
                "Save changes \n"+
                    title.substring(0, title.length()-1),"Close",1);
        if(conf==0) {
            if(directories.get(select).compareTo("No saved")==0) {
                int fileO = fileDialog.showSaveDialog(this);
                if(fileO == JFileChooser.APPROVE_OPTION) {
                    File fo = new File(fileDialog.getSelectedFile().getAbsolutePath()
                     + "." + PL_Constants.EXT_FILES);
                    if(fo.isFile()) {
                        JOptionPane.showMessageDialog(this, 
                            "File exists", "Save As Error", 0);
                        return -1;
                    }
                    this.newSave(fo, select);
                    if(this.getTabCount()==0) {
                        System.exit(0);
                    }
                } else {
                    return 2;
                }
            }
            else if(title.charAt(title.length()-1) == '*') {
            this.saveTabV(select);
            this.removeTabAT(select);
                if(this.getTabCount() == 0) {
                    System.exit(0);
                }
            } 
        }
        else if(conf == 1) {
            this.removeTabAT(select);  
            if(this.getTabCount() == 0) {
                System.exit(0);
            }
        }
        else if(conf == 2) {
            return 2;
        }
        return -1;
    }
    
    public boolean notFileSaved() {
        if(getTabCount() == 0) {
            return true;
        }
        for (int i = getTabCount() - 1; i >= 0; i--) {
            if(closeTabV(i) == 2){
                return false;
            }
        }
        return true;
    }
    
    public void notSave(int select) {//Indicated '*': File no saved
        PL_BarTab tb = (PL_BarTab)getTabComponentAt(select);
        String title = tb.getTabBarTitulo();
        if(title.charAt(title.length()-1) == '*') {
            return;
        }
        title = title + "*";
        //this.setTitleAt(select, title);
        tb.setTabBarTitulo(title);
        file[2].setEnabled(true);
        button[2].setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	//buttons
        int select = this.getSelectedIndex();
        
	if(e.getActionCommand().compareTo("new") == 0) {
            this.newTabAT();
            this.setSelectedIndex(this.getTabCount()-1);
	}
	else if(e.getActionCommand().compareTo("open") == 0) {
            fileDialog.setSelectedFile(new File(""));
            int fileI = fileDialog.showOpenDialog(this);
            if(fileI == JFileChooser.APPROVE_OPTION) {
               File in = fileDialog.getSelectedFile();
               if(PL_Aditional.existDirectory(directories,
                       in.getAbsolutePath())>=0) {
                   JOptionPane.showMessageDialog(this, "File is opened" ,
                           "Open Error", 0);
                   return;
               }
               try {
                    FileReader fr = new FileReader(in);
                    BufferedReader br = new BufferedReader(fr);
                    this.newTabTA(in.getAbsolutePath(), in.getName());
                    //this.setTitleAt(this.getTabCount()-1,in.getName());
                    String line = br.readLine(),text = "";
                    while(line!=null) {
                        text += line+"\n";
                        line = br.readLine();
                    }
                    tabsAreaText.get(this.getTabCount()-1).setText(text);
                    tabsAreaText.get(this.getTabCount()-1).setState(true);
                    file[2].setEnabled(false); button[2].setEnabled(false);
                    fr.close();
                    br.close();
		}
		catch(IOException ex) {
                    JOptionPane.showMessageDialog(this, "Error in open file" ,
                           "Open File", 0);
		}
                this.setSelectedIndex(this.getTabCount()-1);
	     }
          }

	  else if(e.getActionCommand().compareTo("save")==0) {
            if(select<0) {
                return;
            }
            PL_BarTab tb = (PL_BarTab)getTabComponentAt(select);
            String title = tb.getTabBarTitulo();
            if(directories.get(select).compareTo("No saved")==0) {
               fileDialog.setSelectedFile(new File(getTitleNewTab(title)));
               int fileO = fileDialog.showSaveDialog(this);
               if(fileO == JFileChooser.APPROVE_OPTION)
               {                  
                  File fo = new File(fileDialog.getSelectedFile().getAbsolutePath());
                  if(!hasPrefixFile(fo.getName())){
                      fo = new File(fileDialog.getSelectedFile().getAbsolutePath()
                              + "." +PL_Constants.EXT_FILES);
                  }
                  if(fo.isFile()) {
                     JOptionPane.showMessageDialog(this, "File exists",
                             "Save As Error", 0);
                     return;
                  }
                  this.newSave(fo,select);
               }
            }
            else if(title.charAt(title.length()-1)=='*') {
               this.saveTabV(select);
               //this.setTitleAt(select, title.substring(0, title.length()-1));
               tb.setTabBarTitulo(title.substring(0, title.length()-1));
            }
	}
	else if(e.getActionCommand().compareTo("save as")==0) {
            if(select<0) {
                return;
            }
            int fs = fileDialog.showSaveDialog(this);
            if(fs==JFileChooser.APPROVE_OPTION) {
                File fsave = new File(
                        fileDialog.getSelectedFile().getAbsoluteFile()+"");
                if(fsave.isFile()) {
                  JOptionPane.showMessageDialog(this, "File exists",
                          "Save As Error", 0);
                  return;
                }
                try {
                    BufferedWriter bw = new BufferedWriter(
                            new FileWriter(fsave.getAbsoluteFile()));
                    String text = tabsAreaText.get(select).getText();
                    bw.write(text);
                    bw.close();
                }
                catch (IOException ex) {
                    System.out.println("Exception: "+ ex);
                }
            }
	}
	else if(e.getActionCommand().compareTo("close")==0) {
            this.closeTabV(select);
	}
        else if(e.getActionCommand().compareTo("copy")==0) {
            if(tabsAreaText.get(select).getSelectedText()!= null) {
                tabsAreaText.get(select).copy();
            }
        }
        else if(e.getActionCommand().compareTo("cut")==0) {
            if(select>=0) {
               if(tabsAreaText.get(select).getSelectedText()!= null) {
                  tabsAreaText.get(select).cut();
                  tabsAreaText.get(select).setState(true);
                  this.notSave(select);
               }
            }
        }
        else if(e.getActionCommand().compareTo("paste")==0) {
            if(select>=0) {
               tabsAreaText.get(select).paste();
               tabsAreaText.get(select).setState(true);
               this.notSave(select);
            }
        }
        else if(e.getActionCommand().compareTo("all")==0) {
            if(select>=0) {
                if(tabsAreaText.get(select).getText()!=null) {
                    tabsAreaText.get(select).selectAll();
                }
            }
        }
	else if(e.getActionCommand().compareTo("exit")==0)
	{
            if(notFileSaved()) {
                System.exit(0);
            }
	}
        else if(e.getActionCommand().compareTo("menu")==0) {

            barM.setVisible(view[0].isSelected());
	}
	else if(e.getActionCommand().compareTo("tool")==0) {
            barH.setVisible(view[1].isSelected());
	}
	else if(e.getActionCommand().compareTo("status")==0) {
            barE.setVisible(view[2].isSelected());
	}
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int select = getSelectedIndex();
        if(select >=0) {
            file[2].setEnabled(!tabsAreaText.get(select).getState());
            button[2].setEnabled(!tabsAreaText.get(select).getState());
            
            dir.setText(directories.get(select));
            
            zoom.setValue(tabsAreaText.get(select).getLengthFont());
        }
        else {
            zoom.setValue(15);
        }
    }
    
    private String getTitleNewTab(String title){
        int index = title.lastIndexOf(".");
        if(index != -1) {
            return title.substring(0, index);
        }
        return title;
    }
    private boolean hasPrefixFile(String t){
        int index = t.lastIndexOf(".");
        if(index != -1 && index < (t.length() - 1)) {
            String aux = t.substring(index + 1, t.length());
            if(aux.compareTo(PL_Constants.EXT_FILES) == 0){
                return true;
            } 
            return false;
        }
        return false;
    }
    
    public int positionTab(int id) {
        int i = 0;
        for(PL_AreaText at : tabsAreaText) {
            if(at.getId() == id) {
                return i;
            }
            i++;
        }
        return -1;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sampletexteditor.ui;

/**
 *
 * @author Pedro Luis
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import sampletexteditor.utils.PL_Resources;

public class PL_AreaText extends JTextArea 
    implements CaretListener, ActionListener, KeyListener {
    private JPopupMenu menuContex; //Creamos un menu contextual
    private JMenuItem edit[]; 
    private Integer lengthFont;//Tamaño de fuente
    private boolean state;//Estado [Guardado/No guardado] del fichero abierto
    private PL_PanelTabs parent;
    private int id;
    
    public PL_AreaText(PL_PanelTabs parent, int id) {
        super();
        this.id = id;
        this.initAreaTextoE();
        this.parent = parent;
    }

    private void initAreaTextoE() {
        this.lengthFont = 15;
        this.state = false;
        this.setMargin(new Insets(25, 50, 25, 25));
        this.setFont(new Font(Font.DIALOG_INPUT, Font.PLAIN, lengthFont));
        this.initMenuContext();
        this.setComponentPopupMenu(menuContex);
        this.addKeyListener(this);
        this.addCaretListener(this);
    }

    private void initMenuContext() {
        menuContex = new JPopupMenu();
        menuContex.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
        edit = new JMenuItem[4];

        edit[0] = new JMenuItem("Copy");edit[0].setActionCommand("copy");
        edit[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        edit[0].setIcon(new ImageIcon(PL_Resources.getIconResource("Copy16.gif")));
        edit[0].addActionListener(this);

        edit[1] = new JMenuItem("Cut");edit[1].setActionCommand("cut");
        edit[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        edit[1].setIcon(new ImageIcon(PL_Resources.getIconResource("Cut16.gif")));
        edit[1].addActionListener(this);

        edit[2] = new JMenuItem("Paste");edit[2].setActionCommand("paste");
        edit[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
        edit[2].setIcon(new ImageIcon(PL_Resources.getIconResource("Paste16.gif")));
        edit[2].addActionListener(this);

        edit[3] = new JMenuItem("Select All");edit[3].setActionCommand("all");
        edit[3].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        edit[3].addActionListener(this);

        menuContex.add(edit[0]); menuContex.add(edit[1]); menuContex.add(edit[2]);
        menuContex.addSeparator();menuContex.add(edit[3]);
    }

    public void setLengthFont(Integer lengthFont) {
        this.lengthFont = lengthFont;
        this.setFont(new Font(Font.DIALOG_INPUT, Font.PLAIN, lengthFont));
    }

    public Integer getLengthFont() {
        return lengthFont;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean getState() {
        return state;
    }
    
    public int getId(){
        return id;
    }
    
    private boolean isEdited(KeyEvent e) {
        return !e.isActionKey() && !e.isAltDown() &&
               !e.isAltGraphDown() && !e.isControlDown() &&
               !e.isShiftDown();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().compareTo("copy")==0) {
            if(this.getSelectedText()!=null) {
                this.copy();
            }
        }
        else if(e.getActionCommand().compareTo("cut")==0) {
            if(this.getSelectedText()!=null) {
                this.cut(); state = false;
            }
        }
        else if(e.getActionCommand().compareTo("paste")==0) {
            this.paste(); state = false;
        }
        else if(e.getActionCommand().compareTo("all")==0) {
            this.selectAll();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
       if(this.state && this.isEdited(e)) {
           this.state = false;
           parent.notSave(parent.getSelectedIndex());
       }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
    @Override
    public void caretUpdate(CaretEvent e) {
    }
    
}

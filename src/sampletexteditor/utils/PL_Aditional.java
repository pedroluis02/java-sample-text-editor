/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sampletexteditor.utils;

/**
 *
 * @author Pedro Luis
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class PL_Aditional {
    
    public static int existDirectory(ArrayList <String> dirs, String dir) {
        ArrayList <String> aux = new ArrayList();
        Iterator <String> it = dirs.iterator();
        while(it.hasNext()) {
            aux.add(it.next());
        }
        Collections.sort(aux);
        return Collections.binarySearch(aux, dir);
    }
    
}

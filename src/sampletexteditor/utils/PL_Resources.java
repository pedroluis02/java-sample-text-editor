/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sampletexteditor.utils;

import java.net.URL;

/**
 *
 * @author Pedro Luis
 */
public class PL_Resources {
    
    private static final String PL_BASE_RESOURCE = "sampletexteditor/ui/";
    
    public static URL getIconResource (String name) {
        return PL_Resources.class.getClassLoader()
                .getResource(PL_BASE_RESOURCE + "icons/" + name);
    }
    
}
